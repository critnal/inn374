/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.ImageIcon;

/**
 *
 * @author alexj_000
 */
public class ANDGate extends BinaryGate {

    public ANDGate() {
        super("images/AND.gif");
    }

    @Override
    protected boolean Compute(boolean a, boolean b) {
        return a && b;
    }
}
