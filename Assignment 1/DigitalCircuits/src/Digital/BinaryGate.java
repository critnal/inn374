/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author alexj_000
 */
public abstract class BinaryGate extends javax.swing.JPanel implements PropertyChangeListener {

    private Image image;
    private OutputTerminal output = new OutputTerminal();
    private Terminal input0;
    private Terminal input1;

    public BinaryGate(String imageName) {
        java.net.URL url = getClass().getResource(imageName);
        image = new javax.swing.ImageIcon(url).getImage();
        this.setSize(image.getWidth(null), image.getHeight(null));
    }

    @Override
    public void paintComponent(java.awt.Graphics g) {
        g.drawImage(image, 0, 0, null);
    }
    
    public Terminal getOutput() {
        return output;
    }
    
    public Terminal getInput0() {
        return input0;
    }
    
    public void setInput0(Terminal terminal) {
        input0 = terminal;
        if (terminal != null) {
            terminal.addPropertyChangeListener(this);            
        }
        recompute();
    }
    
    public Terminal getInput1() {
        return input1;
    }
    
    public void setInput1(Terminal terminal) {
        input1 = terminal;
        if (terminal != null) {
            terminal.addPropertyChangeListener(this);            
        }
        recompute();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        recompute();
    }
    
    private void recompute() {
        boolean a = (input0 == null ? false : input0.getValue());
        boolean b = (input1 == null ? false : input1.getValue());
        output.setValue(Compute(a, b));
    }
    
    protected abstract boolean Compute(boolean a, boolean b);
}
