/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Digital;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author alexj_000
 */
public class LED  extends javax.swing.JPanel implements PropertyChangeListener {

    private Image imageOff;
    private Image imageOn;
    private Terminal input0;

    public LED() {
        java.net.URL url = getClass().getResource("images/LED_off.gif");
        imageOff = new javax.swing.ImageIcon(url).getImage();
        url = getClass().getResource("images/LED_on.gif");
        imageOn = new javax.swing.ImageIcon(url).getImage();
        this.setSize(imageOff.getWidth(null), imageOff.getHeight(null));
    }
    
    public void setInput0(Terminal terminal) {
        input0 = terminal;
        if (terminal != null) {
            terminal.addPropertyChangeListener(this);            
        }
    }
    
    public Terminal getInput0() {
        return input0;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        repaint();
    }    

    @Override
    public void paintComponent(java.awt.Graphics g) {
        Image image;
        if (input0 == null) {
            image = imageOff;
        } else {
            image = input0.getValue() ? imageOn : imageOff;
        }
        g.drawImage(image, 0, 0, null);
    }
}
