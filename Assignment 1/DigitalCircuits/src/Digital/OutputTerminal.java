/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Digital;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author alexj_000
 */
public class OutputTerminal implements Terminal {
    
    private boolean value;
    private PropertyChangeSupport mPcs =
        new PropertyChangeSupport(this);

    @Override
    public boolean getValue() {
        return value;
    }
    
    public void setValue(boolean newValue) {
        boolean oldValue = value;
        value = newValue;
        mPcs.firePropertyChange("Value",
                                   oldValue, newValue);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener pl) {
        mPcs.addPropertyChangeListener(pl);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener pl) {
        mPcs.removePropertyChangeListener(pl);
    }
    
}
