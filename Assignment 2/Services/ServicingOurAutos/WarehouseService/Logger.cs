﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

public class Logger
{
    protected static readonly string fileName = "Log.txt";

    public static void Log(string text)
    {
        File.AppendAllText(fileName, text + "\n");
    }
}