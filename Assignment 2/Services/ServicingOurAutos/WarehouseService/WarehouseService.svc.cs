﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;

namespace WarehouseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class WarehouseService : IWarehouseService
    {
        protected SqlConnection newConnection()
        {
            return new SqlConnection("Data Source=fastapps04.qut.edu.au;Initial Catalog=n6878296;User ID=n6878296;Password=password");
        }

        public Stock GetStock(int id)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Stock WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", id);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string name = reader.GetString(1);
                        int quantityIn = reader.GetInt32(2);
                        int quantityOrdered = reader.GetInt32(3);
                        int quantityBooked = reader.GetInt32(4);
                        //Logger.Log(string.Format("GetStock(): {0}, {1}, {2}, {3}, {4}", id, name, quantityIn,
                        //    quantityOrdered, quantityBooked));
                        return new Stock(id, name, quantityIn, quantityOrdered, quantityBooked);
                    }
                    return null;
                }
            }
        }

        public void UpdateStock(Stock stock)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("UPDATE Stock SET quantity_in = @quantity_in, "
                    + "quantity_ordered = @quantity_ordered, quantity_booked = @quantity_booked WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", stock.ID);
                command.Parameters.AddWithValue("@quantity_in", stock.QuantityIn);
                command.Parameters.AddWithValue("@quantity_ordered", stock.QuantityOrdered);
                command.Parameters.AddWithValue("@quantity_booked", stock.QuantityBooked);
                command.ExecuteNonQuery();
            }
        }

        public Service GetService(string serviceType)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Services WHERE service_type = @service_type", connection);
                command.Parameters.AddWithValue("@service_type", serviceType);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Service service = new Service(serviceType);
                    while (reader.Read())
                    {
                        service.ItemIDs.Add(reader.GetInt32(1));
                    }
                    return service;
                }
            }
        }

        public List<int> GetStockMissingForService(string serviceType)
        {
            Service service = GetService(serviceType);
            List<int> missingStockIDs = new List<int>();

            foreach (int itemID in service.ItemIDs)
            {
                Stock stock = GetStock(itemID);
                int totalAvailable = stock.QuantityIn - stock.QuantityBooked;
                //Logger.Log(string.Format("GetStock(): {0}, {1}, {2}, {3}, {4}",
                //    stock.ID, stock.Name, stock.QuantityIn, stock.QuantityOrdered, stock.QuantityBooked));
                if (totalAvailable <= 0)
                {
                    missingStockIDs.Add(itemID);
                }
            }
            return missingStockIDs;
        }

        public void BookStockForService(string serviceType)
        {
            Service service = GetService(serviceType);

            foreach (int itemID in service.ItemIDs)
            {
                Stock stock = GetStock(itemID);
                stock.QuantityBooked++;
                UpdateStock(stock);
            }
        }
    }
}
