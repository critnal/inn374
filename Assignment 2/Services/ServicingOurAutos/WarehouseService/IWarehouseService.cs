﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WarehouseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IWarehouseService
    {

        [OperationContract]
        Stock GetStock(int itemID);

        [OperationContract]
        void UpdateStock(Stock stock);

        [OperationContract]
        Service GetService(string serviceType);

        [OperationContract]
        List<int> GetStockMissingForService(string serviceType);

        [OperationContract]
        void BookStockForService(string serviceType);
    }

    [DataContract]
    public class Stock
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int QuantityIn { get; set; }

        [DataMember]
        public int QuantityOrdered { get; set; }

        [DataMember]
        public int QuantityBooked { get; set; }

        public Stock(int id, string name, int quantityIn, int quantityOrdered, int quantityBooked)
        {
            this.ID = id;
            this.Name = name;
            this.QuantityIn = quantityIn;
            this.QuantityOrdered = quantityOrdered;
            this.QuantityBooked = quantityBooked;
        }
    }

    [DataContract]
    public class Service
    {
        [DataMember]
        public string ServiceType { get; set; }

        [DataMember]
        public List<int> ItemIDs { get; set; }

        public Service(string serviceType, List<int> itemIDs)
        {
            this.ServiceType = serviceType;
            this.ItemIDs = itemIDs;
        }

        public Service(string serviceType)
        {
            this.ServiceType = serviceType;
            this.ItemIDs = new List<int>();
        }
    }
}
