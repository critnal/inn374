﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServicingOurAutos.BookingService;

namespace ServicingOurAutos
{
    [ServiceContract]
    public interface ISalesService
    {
        [OperationContract]
        [WebInvoke(
            UriTemplate = "RequestQuote/{vehicleRegistration}/{brand}/{serviceType}",
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        Booking RequestQuote(string vehicleRegistration, string brand, string serviceType);

        [OperationContract]
        [WebInvoke(
            UriTemplate = "AcceptQuote/{bookingID}",
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        Delay AcceptQuote(string bookingID);

        [OperationContract]
        [WebInvoke(
            UriTemplate = "CancelBooking/{bookingID}",
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        void CancelBooking(string bookingID);

        [OperationContract]
        [WebInvoke(
            UriTemplate = "BookService/{bookingID}",
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        Response BookService(string bookingID);
    }
}
