﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicingOurAutos.BookingService;
using ServicingOurAutos.InsuranceService;
using ServicingOurAutos.GarageService;
using ServicingOurAutos.WarehouseService;
using System.Net;
using System.Xml.Linq;
using System.IO;


namespace ServicingOurAutos
{
    public class SalesService : ISalesService
    {
        protected const string SUPPLIER_URL = "http://fastws.qut.edu.au:8080/N6878296_Supplier/SupplierService/";

        public Booking RequestQuote(string vehicleRegistration, string brand, string serviceType)
        {
            InsuranceServiceClient insuranceService = new InsuranceServiceClient();
            BookingServiceClient bookingService = new BookingServiceClient();

            int id = bookingService.NewBookingID();
            int cost = getCostOfService(serviceType);
            int amountOwed = (int)insuranceService.GetAmountOwed(cost, brand);
            Booking booking = new Booking()
            {
                ID = id,
                Status = "Quote",
                VehicleRegistration = vehicleRegistration,
                Brand = brand,
                ServiceType = serviceType,
                Cost = cost,
                AmountOwed = amountOwed,
                Date = DateTime.Today
            };

            bookingService.SaveBooking(booking);
            bookingService.Close();
            insuranceService.Close();
            return booking;
        }

        protected int getCostOfService(string serviceType)
        {
            if (serviceType.Equals("Basic", StringComparison.OrdinalIgnoreCase))
            {
                return 200;
            }
            else if (serviceType.Equals("Medium", StringComparison.OrdinalIgnoreCase))
            {
                return 800;
            }
            else
            {
                return 1600;
            }
        }

        public Delay AcceptQuote(string bookingIDString)
        {
            BookingServiceClient bookingService = new BookingServiceClient();
            int bookingID = int.Parse(bookingIDString);
            Booking booking = bookingService.GetBooking(bookingID);            
            bookingService.Close();

            if (allPartsAvailable(booking))
            {
                return new Delay(0);
            }
            else
            {
                return getDelay(booking);
            }

        }

        protected bool allPartsAvailable(Booking booking)
        {
            using (WarehouseServiceClient warehouseService = new WarehouseServiceClient())
            {
                int count = warehouseService.GetStockMissingForService(booking.ServiceType).Length;
                return count == 0;
            }
        }

        protected Delay getDelay(Booking booking)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                new Uri(SUPPLIER_URL + "Stock/GetOrderDeliveryTime"));
            request.Method = "POST";
            request.ContentType = "application/xml";
            request.Accept = "application/xml";

            XDocument orderXML = makeOrderXML(getMissingStockQuantities(booking));

            //string xmlString = @"<?xml version=""1.0""?><order><entry><quantity>3</quantity><stockItem>Tyre</stockItem></entry></order>";
            //XDocument xml = XDocument.Parse(xmlString);

            byte[] bytes = Encoding.UTF8.GetBytes(orderXML.ToString());

            request.ContentLength = bytes.Length;

            using (Stream putStream = request.GetRequestStream())
            {
                putStream.Write(bytes, 0, bytes.Length);
            }
            try 
            { 
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xmlResponse = XDocument.Parse(reader.ReadToEnd());
                    return new Delay(int.Parse(xmlResponse.Descendants().FirstOrDefault(e => e.Name == "days").Value));

                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
            }
            return new Delay(0);

        }

        public void CancelBooking(string bookingIDString)
        {
            BookingServiceClient bookingService = new BookingServiceClient();
            int bookingID = int.Parse(bookingIDString);
            bookingService.RemoveBooking(bookingID);
            bookingService.Close();
        }


        public Response BookService(string bookingIDString)
        {
            using (BookingServiceClient bookingService = new BookingServiceClient())
            using (GarageServiceClient garageService = new GarageServiceClient())
            using (WarehouseServiceClient warehouseService = new WarehouseServiceClient())
            {
                // Enter booking
                int bookingID = int.Parse(bookingIDString);
                Booking booking = bookingService.GetBooking(bookingID);
                booking.Date = booking.Date.AddDays(getDelay(booking).Days);
                booking.Status = "Booked";
                bookingService.SaveBooking(booking);

                // Assign employee to do work
                foreach (Employee employee in garageService.GetAllEmployees())
                {
                    if (garageService.IsQualifiedForServiceType(employee.ID, booking.ServiceType))
                    {
                        int jobID = garageService.NewJobID();
                        garageService.SaveJob(new Job()
                        {
                            ID = jobID,
                            BookingID = booking.ID,
                            EmployeeID = employee.ID
                        });
                        break;
                    }
                }

                // Book stock
                warehouseService.BookStockForService(booking.ServiceType);

                // Order any missing stock
                XDocument orderXML = makeOrderXML(getMissingStockQuantities(booking));
                if (placeOrder(orderXML))
                {
                    return new Response("Your vehicle has been booked in for servicing on " + booking.Date.ToShortDateString());
                }
                return new Response("There was a problem booking your service. Please try again later.");
            }
        }

        protected Dictionary<Stock, int> getMissingStockQuantities(Booking booking)
        {
            using (WarehouseServiceClient warehouseService = new WarehouseServiceClient())
            {
                int[] missingStockIDs = warehouseService.GetStockMissingForService(booking.ServiceType);
                Dictionary<Stock, int> stockQuantities = new Dictionary<Stock, int>();
                foreach (int stockID in missingStockIDs)
                {
                    Stock stock = warehouseService.GetStock(stockID);
                    int quantityToOrder = stock.QuantityBooked - stock.QuantityIn - stock.QuantityOrdered;
                    if (quantityToOrder > 0)
                    {
                        stockQuantities.Add(stock, quantityToOrder);
                    }
                }
                return stockQuantities;
            }            
        }

        protected XDocument makeOrderXML(Dictionary<Stock, int> stockQuantities)
        {
            return new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("order",
                    stockQuantities.Select(i => new XElement("entry",
                        new XElement("stockItem", i.Key.Name),
                        new XElement("quantity", i.Value)))));
        }

        protected bool placeOrder(XDocument orderXML)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                new Uri(SUPPLIER_URL + "Stock/PlaceOrder"));
            request.Method = "POST";
            request.ContentType = "application/xml";
            request.Accept = "application/xml";

            //string xmlString = @"<?xml version=""1.0""?><order><entry><quantity>3</quantity><stockItem>Tyre</stockItem></entry></order>";
            //XDocument xml = XDocument.Parse(xmlString);

            byte[] bytes = Encoding.UTF8.GetBytes(orderXML.ToString());

            request.ContentLength = bytes.Length;

            using (Stream putStream = request.GetRequestStream())
            {
                putStream.Write(bytes, 0, bytes.Length);
            }
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xmlResponse = XDocument.Parse(reader.ReadToEnd());
                    return (xmlResponse.Descendants().FirstOrDefault(e => e.Name == "message").Value) == "Order Successful";
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
            }
            return false;
        }
    }
}
