﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicingOurAutos.InsuranceService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="InsuranceService.IInsuranceService")]
    public interface IInsuranceService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IInsuranceService/GetAmountOwed", ReplyAction="http://tempuri.org/IInsuranceService/GetAmountOwedResponse")]
        float GetAmountOwed(float cost, string brand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IInsuranceService/GetAmountOwed", ReplyAction="http://tempuri.org/IInsuranceService/GetAmountOwedResponse")]
        System.Threading.Tasks.Task<float> GetAmountOwedAsync(float cost, string brand);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IInsuranceServiceChannel : ServicingOurAutos.InsuranceService.IInsuranceService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class InsuranceServiceClient : System.ServiceModel.ClientBase<ServicingOurAutos.InsuranceService.IInsuranceService>, ServicingOurAutos.InsuranceService.IInsuranceService {
        
        public InsuranceServiceClient() {
        }
        
        public InsuranceServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public InsuranceServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public InsuranceServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public InsuranceServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public float GetAmountOwed(float cost, string brand) {
            return base.Channel.GetAmountOwed(cost, brand);
        }
        
        public System.Threading.Tasks.Task<float> GetAmountOwedAsync(float cost, string brand) {
            return base.Channel.GetAmountOwedAsync(cost, brand);
        }
    }
}
