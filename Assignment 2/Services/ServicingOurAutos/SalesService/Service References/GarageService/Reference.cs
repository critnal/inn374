﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServicingOurAutos.GarageService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Employee", Namespace="http://schemas.datacontract.org/2004/07/GarageService")]
    [System.SerializableAttribute()]
    public partial class Employee : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Job", Namespace="http://schemas.datacontract.org/2004/07/GarageService")]
    [System.SerializableAttribute()]
    public partial class Job : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int BookingIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int EmployeeIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int BookingID {
            get {
                return this.BookingIDField;
            }
            set {
                if ((this.BookingIDField.Equals(value) != true)) {
                    this.BookingIDField = value;
                    this.RaisePropertyChanged("BookingID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int EmployeeID {
            get {
                return this.EmployeeIDField;
            }
            set {
                if ((this.EmployeeIDField.Equals(value) != true)) {
                    this.EmployeeIDField = value;
                    this.RaisePropertyChanged("EmployeeID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="GarageService.IGarageService")]
    public interface IGarageService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetEmployee", ReplyAction="http://tempuri.org/IGarageService/GetEmployeeResponse")]
        ServicingOurAutos.GarageService.Employee GetEmployee(int employeeID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetEmployee", ReplyAction="http://tempuri.org/IGarageService/GetEmployeeResponse")]
        System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Employee> GetEmployeeAsync(int employeeID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetAllEmployees", ReplyAction="http://tempuri.org/IGarageService/GetAllEmployeesResponse")]
        ServicingOurAutos.GarageService.Employee[] GetAllEmployees();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetAllEmployees", ReplyAction="http://tempuri.org/IGarageService/GetAllEmployeesResponse")]
        System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Employee[]> GetAllEmployeesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/IsQualifiedForServiceType", ReplyAction="http://tempuri.org/IGarageService/IsQualifiedForServiceTypeResponse")]
        bool IsQualifiedForServiceType(int employeeID, string serviceType);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/IsQualifiedForServiceType", ReplyAction="http://tempuri.org/IGarageService/IsQualifiedForServiceTypeResponse")]
        System.Threading.Tasks.Task<bool> IsQualifiedForServiceTypeAsync(int employeeID, string serviceType);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/NewJobID", ReplyAction="http://tempuri.org/IGarageService/NewJobIDResponse")]
        int NewJobID();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/NewJobID", ReplyAction="http://tempuri.org/IGarageService/NewJobIDResponse")]
        System.Threading.Tasks.Task<int> NewJobIDAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/SaveJob", ReplyAction="http://tempuri.org/IGarageService/SaveJobResponse")]
        void SaveJob(ServicingOurAutos.GarageService.Job job);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/SaveJob", ReplyAction="http://tempuri.org/IGarageService/SaveJobResponse")]
        System.Threading.Tasks.Task SaveJobAsync(ServicingOurAutos.GarageService.Job job);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetJob", ReplyAction="http://tempuri.org/IGarageService/GetJobResponse")]
        ServicingOurAutos.GarageService.Job GetJob(int jobID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetJob", ReplyAction="http://tempuri.org/IGarageService/GetJobResponse")]
        System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Job> GetJobAsync(int jobID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetAllJobs", ReplyAction="http://tempuri.org/IGarageService/GetAllJobsResponse")]
        ServicingOurAutos.GarageService.Job[] GetAllJobs();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGarageService/GetAllJobs", ReplyAction="http://tempuri.org/IGarageService/GetAllJobsResponse")]
        System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Job[]> GetAllJobsAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IGarageServiceChannel : ServicingOurAutos.GarageService.IGarageService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GarageServiceClient : System.ServiceModel.ClientBase<ServicingOurAutos.GarageService.IGarageService>, ServicingOurAutos.GarageService.IGarageService {
        
        public GarageServiceClient() {
        }
        
        public GarageServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public GarageServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GarageServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GarageServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ServicingOurAutos.GarageService.Employee GetEmployee(int employeeID) {
            return base.Channel.GetEmployee(employeeID);
        }
        
        public System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Employee> GetEmployeeAsync(int employeeID) {
            return base.Channel.GetEmployeeAsync(employeeID);
        }
        
        public ServicingOurAutos.GarageService.Employee[] GetAllEmployees() {
            return base.Channel.GetAllEmployees();
        }
        
        public System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Employee[]> GetAllEmployeesAsync() {
            return base.Channel.GetAllEmployeesAsync();
        }
        
        public bool IsQualifiedForServiceType(int employeeID, string serviceType) {
            return base.Channel.IsQualifiedForServiceType(employeeID, serviceType);
        }
        
        public System.Threading.Tasks.Task<bool> IsQualifiedForServiceTypeAsync(int employeeID, string serviceType) {
            return base.Channel.IsQualifiedForServiceTypeAsync(employeeID, serviceType);
        }
        
        public int NewJobID() {
            return base.Channel.NewJobID();
        }
        
        public System.Threading.Tasks.Task<int> NewJobIDAsync() {
            return base.Channel.NewJobIDAsync();
        }
        
        public void SaveJob(ServicingOurAutos.GarageService.Job job) {
            base.Channel.SaveJob(job);
        }
        
        public System.Threading.Tasks.Task SaveJobAsync(ServicingOurAutos.GarageService.Job job) {
            return base.Channel.SaveJobAsync(job);
        }
        
        public ServicingOurAutos.GarageService.Job GetJob(int jobID) {
            return base.Channel.GetJob(jobID);
        }
        
        public System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Job> GetJobAsync(int jobID) {
            return base.Channel.GetJobAsync(jobID);
        }
        
        public ServicingOurAutos.GarageService.Job[] GetAllJobs() {
            return base.Channel.GetAllJobs();
        }
        
        public System.Threading.Tasks.Task<ServicingOurAutos.GarageService.Job[]> GetAllJobsAsync() {
            return base.Channel.GetAllJobsAsync();
        }
    }
}
