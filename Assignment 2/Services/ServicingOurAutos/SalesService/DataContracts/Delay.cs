﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServicingOurAutos
{
    [DataContract]
    public class Delay
    {
        [DataMember]
        public int Days { get; set; }

        public Delay() { }

        public Delay(int days)
        {
            this.Days = days;
        }
    }
}