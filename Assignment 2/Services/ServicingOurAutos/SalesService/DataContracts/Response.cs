﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServicingOurAutos
{
    [DataContract]
    public class Response
    {
        [DataMember]
        public string Message { get; set; }

        public Response() { }

        public Response(string message)
        {
            this.Message = message;
        }
    }
}