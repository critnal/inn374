﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServicingOurAutos
{
    [DataContract]
    public class Booking
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string VehicleRegistration { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ServiceType { get; set; }

        [DataMember]
        public int Cost { get; set; }

        [DataMember]
        public int AmountOwed { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        public Booking() { }

        public Booking(int id, string status, string vehicleRegistration, string brand, 
            string serviceType, int cost, int amountOwed, DateTime date)
        {
            this.ID = id;
            this.Status = status;
            this.VehicleRegistration = vehicleRegistration;
            this.Brand = brand;
            this.ServiceType = serviceType;
            this.Cost = cost;
            this.AmountOwed = amountOwed;
            this.Date = date;
        }
    }
}