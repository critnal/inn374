﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.SqlClient;

namespace ServicingOurAutos
{
    public class BookingService : IBookingService
    {
        protected SqlConnection newConnection()
        {
            return new SqlConnection("Data Source=fastapps04.qut.edu.au;Initial Catalog=n6878296;User ID=n6878296;Password=password");
        }
        
        public int NewBookingID()
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT TOP 1 id FROM Bookings ORDER BY id DESC", connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return reader.GetInt32(0) + 1;
                    }
                    return 1;
                }
            }
        }

        public void SaveBooking(Booking booking)
        {
            if (DoesBookingExist(booking.ID))
            {
                updateBooking(booking);
            }
            else
            {
                insertBooking(booking);
            }
        }

        public bool DoesBookingExist(int bookingID)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT id FROM Bookings WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", bookingID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    return reader.HasRows;
                }
            }
        }

        protected void updateBooking(Booking booking)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("UPDATE Bookings SET status=@status, vehicle_registration=@vehicle_registration, "
                    + "service_type=@service_type, brand=@brand, cost=@cost, amount_owed=@amount_owed, date=@date WHERE id=@id",
                    connection);
                command.Parameters.AddWithValue("@id", booking.ID);
                command.Parameters.AddWithValue("@status", booking.Status);
                command.Parameters.AddWithValue("@vehicle_registration", booking.VehicleRegistration);
                command.Parameters.AddWithValue("@brand", booking.Brand);
                command.Parameters.AddWithValue("@service_type", booking.ServiceType);
                command.Parameters.AddWithValue("@cost", booking.Cost);
                command.Parameters.AddWithValue("@amount_owed", booking.AmountOwed);
                command.Parameters.AddWithValue("@date", booking.Date);
                command.ExecuteNonQuery();
            }
        }

        protected void insertBooking(Booking booking)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("INSERT INTO Bookings VALUES (@id, @status, @vehicle_registration, "
                    + "@brand, @service_type, @cost, @amount_owed, @date)",
                    connection);
                command.Parameters.AddWithValue("@id", booking.ID);
                command.Parameters.AddWithValue("@status", booking.Status);
                command.Parameters.AddWithValue("@vehicle_registration", booking.VehicleRegistration);
                command.Parameters.AddWithValue("@cost", booking.Cost);
                command.Parameters.AddWithValue("@amount_owed", booking.AmountOwed);
                command.Parameters.AddWithValue("@service_type", booking.ServiceType);
                command.Parameters.AddWithValue("@brand", booking.Brand);
                command.Parameters.AddWithValue("@date", booking.Date);
                command.ExecuteNonQuery();
            }
        }

        public Booking GetBooking(int bookingID)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Bookings WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", bookingID);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string status = reader.GetString(1);
                        string vehicleRegistration = reader.GetString(2);
                        string brand = reader.GetString(3);
                        string serviceType = reader.GetString(4);
                        int cost = reader.GetInt32(5);
                        int amountOwed = reader.GetInt32(6);
                        DateTime date = reader.GetDateTime(7);
                        return new Booking(bookingID, status, vehicleRegistration, 
                            brand, serviceType, cost, amountOwed, date);
                    }
                    return null;
                }
            }
        }

        public void RemoveBooking(int bookingID)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Bookings WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", bookingID);
                command.ExecuteNonQuery();
            }
        }

        public void UpdateBookingStatus(int bookingID, string status)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("UPDATE Bookings SET status=@status WHERE id=@id", connection);
                command.Parameters.AddWithValue("@id", bookingID);
                command.Parameters.AddWithValue("@status", status);
                command.ExecuteNonQuery();
            }
        }
    }
}
