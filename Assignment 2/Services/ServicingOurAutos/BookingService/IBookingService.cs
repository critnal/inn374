﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServicingOurAutos
{
    [ServiceContract]
    public interface IBookingService
    {
        [OperationContract]
        int NewBookingID();

        [OperationContract]
        void SaveBooking(Booking booking);

        [OperationContract]
        bool DoesBookingExist(int bookingID);

        [OperationContract]
        Booking GetBooking(int bookingID);

        [OperationContract]
        void RemoveBooking(int bookingID);

        [OperationContract]
        void UpdateBookingStatus(int bookingID, string status);
    }
}
