﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServicingOurAutos
{
    [ServiceContract]
    public interface IInsuranceService
    {
        [OperationContract]
        float GetAmountOwed(float cost, string brand);
    }
}
