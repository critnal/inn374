﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;

namespace GarageService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class GarageService : IGarageService
    {
        protected SqlConnection newConnection()
        {
            return new SqlConnection("Data Source=fastapps04.qut.edu.au;Initial Catalog=n6878296;User ID=n6878296;Password=password");
        }

        #region Employees

        public Employee GetEmployee(int employeeID)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Employees WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", employeeID);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string name = reader.GetString(1);
                        return new Employee(employeeID, name);
                    }
                    return null;
                }
            }
        }

        public Employee[] GetAllEmployees()
        {
            List<Employee> employees = new List<Employee>();
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Employees", connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        employees.Add(new Employee(id, name));
                    }
                }
            }
            return employees.ToArray();
        }

        public bool IsQualifiedForServiceType(int employeeID, string serviceType)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT employee_id FROM Qualifications WHERE employee_id = @employee_id AND service_type = @service_type", connection);
                command.Parameters.AddWithValue("@employee_id", employeeID);
                command.Parameters.AddWithValue("@service_type", serviceType);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    return reader.HasRows;
                }
            }
        }

        #endregion

        #region Jobs

        public int NewJobID()
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT TOP 1 id FROM Jobs ORDER BY id DESC", connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return reader.GetInt32(0) + 1;
                    }
                    return 1;
                }
            }
        }

        public void SaveJob(Job job)
        {
            if (DoesJobExist(job.ID))
            {
                updateJob(job);
            }
            else
            {
                insertJob(job);
            }
        }

        public bool DoesJobExist(int jobID)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT id FROM Jobs WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", jobID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    return reader.HasRows;
                }
            }
        }

        protected void updateJob(Job job)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("UPDATE Jobs SET booking_id=@booking_id, employee_id=@employee_id WHERE id=@id",
                    connection);
                command.Parameters.AddWithValue("@id", job.ID);
                command.Parameters.AddWithValue("@booking_id", job.BookingID);
                command.Parameters.AddWithValue("@employee_id", job.EmployeeID);
                command.ExecuteNonQuery();
            }
        }

        protected void insertJob(Job job)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("INSERT INTO Jobs VALUES (@id, @booking_id, @employee_id)",
                    connection);
                command.Parameters.AddWithValue("@id", job.ID);
                command.Parameters.AddWithValue("@booking_id", job.BookingID);
                command.Parameters.AddWithValue("@employee_id", job.EmployeeID);
                command.ExecuteNonQuery();
            }
        }

        public Job GetJob(int jobID)
        {
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Jobs WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", jobID);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int bookingID = reader.GetInt32(1);
                        int employeeID = reader.GetInt32(2);
                        return new Job(jobID, bookingID, employeeID);
                    }
                    return null;
                }
            }
        }

        public Job[] GetAllJobs()
        {
            List<Job> jobs = new List<Job>();
            using (SqlConnection connection = newConnection())
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Jobs", connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int jobID = reader.GetInt32(0);
                        int bookingID = reader.GetInt32(1);
                        int employeeID = reader.GetInt32(2);
                        jobs.Add(new Job(jobID, bookingID, employeeID));
                    }
                }
            }
            return jobs.ToArray();
        }

        #endregion
    }
}
