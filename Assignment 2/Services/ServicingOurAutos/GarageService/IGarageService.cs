﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GarageService
{
    [ServiceContract]
    public interface IGarageService
    {
        [OperationContract]
        Employee GetEmployee(int employeeID);

        [OperationContract]
        Employee[] GetAllEmployees();

        [OperationContract]
        bool IsQualifiedForServiceType(int employeeID, string serviceType);

        [OperationContract]
        int NewJobID();

        [OperationContract]
        void SaveJob(Job job);

        [OperationContract]
        Job GetJob(int jobID);

        [OperationContract]
        Job[] GetAllJobs();
    }


    [DataContract]
    public class Employee
    {
        [DataMember]
        int ID { get; set; }

        [DataMember]
        string Name { get; set; }

        public Employee() { }

        public Employee(int id, string name)
        {
            this.ID = id;
            this.Name = name;
        }
    }

    [DataContract]
    public class Job
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int BookingID { get; set; }

        [DataMember]
        public int EmployeeID { get; set; }

        public Job() { }

        public Job(int id, int bookingID, int employeeID)
        {
            this.ID = id;
            this.BookingID = bookingID;
            this.EmployeeID = employeeID;
        }
    }
}
