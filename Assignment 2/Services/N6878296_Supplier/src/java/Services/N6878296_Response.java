/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import javax.xml.bind.annotation.*;

/**
 *
 * @author alexj_000
 */
@XmlRootElement(name = "response")
public class N6878296_Response {
    
    private String message;
    
    @XmlElement(name = "message")
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
}
