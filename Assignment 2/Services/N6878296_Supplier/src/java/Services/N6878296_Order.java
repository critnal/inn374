package Services;

import java.util.List;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "order")
public class N6878296_Order {
    
    @XmlElement(name = "entry")
    private List<N6878296_OrderEntry> entries;
    
    public List<N6878296_OrderEntry> getEntries() {
        return entries;
    }
    
    public void setEntries(List<N6878296_OrderEntry> entries) {
        this.entries = entries;
    }
}

