/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import javax.xml.bind.annotation.*;

/**
 *
 * @author alexj_000
 */

@XmlRootElement(name = "deliveryTime")
public class N6878296_DeliveryTime {
    
    private int days;
    
    public N6878296_DeliveryTime() {}
    
    public N6878296_DeliveryTime(int days) {
        this.days = days;
    }
    
    @XmlElement
    public int getDays() {
        return days;
    }
    
    public void setDays(int days) {
        this.days = days;
    }
}
