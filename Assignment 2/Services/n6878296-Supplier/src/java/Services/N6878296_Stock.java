/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;

import com.mysql.jdbc.Connection;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author alexj_000
 */
@Path("/Stock")
public class N6878296_Stock {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of N6878296_SupplierService
     */
    public N6878296_Stock() {
    }
    
    protected void log(String text) {
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("myfile.txt", true)))) {
            out.println(text);
        }catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    protected Connection newConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://fastws.qut.edu.au:3306/n6878296", "n6878296", "password");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(N6878296_Stock.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }
    
    @GET
    @Path("/DoesStockItemExist/{stockItem}")
    @Produces("application/xml")
    public N6878296_Order DoesStockItemExist(@PathParam("stockItem") String stockItemName) {
        String result = "false";
//        try {
//            Connection connection = newConnection();
//            Statement statement = connection.createStatement();
//            ResultSet results = statement.executeQuery("SELECT * FROM Stock WHERE name == " + stockItem);
//            if (results.next()) {
//                result = "true";
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(N6878296_Stock.class.getName()).log(Level.SEVERE, null, ex);
//        }
        switch (stockItemName) {
            case "Tyre" :
            case "Engine Oil" :
            case "Brake Fluid" :
            case "Fan Belt" :
                result = "true";
                break;
        }
        N6878296_OrderEntry entry = new N6878296_OrderEntry();
        entry.setStockItem("Tyre");
        entry.setQuantity(2);
        
        N6878296_Order order = new N6878296_Order();
        order.setEntries(new ArrayList<N6878296_OrderEntry>());
        order.getEntries().add(entry);
        return order;
    }
    
    @POST
    @Path("/GetOrderDeliveryTime")
    @Consumes("application/xml")
    @Produces("application/xml")
    public N6878296_DeliveryTime GetOrderDeliveryTime(N6878296_Order order) {
        int maxDeliveryTime = 0;
            log(maxDeliveryTime + ": " + maxDeliveryTime);
        for (N6878296_OrderEntry orderEntry : order.getEntries()) {
            maxDeliveryTime = Math.max(maxDeliveryTime, getStockItemDeliveryTime(orderEntry.getStockItem()));
            log(orderEntry + ": " + maxDeliveryTime);
        }
        
        return new N6878296_DeliveryTime(maxDeliveryTime);
    }
    
    protected int getStockItemDeliveryTime(String stockItem) {
        switch (stockItem) {
            case "Tyre" :
                return 5;
            case "Engine Oil" :
            case "Fan Belt" :
                return 3;
            case "Brake Fluid" :
                return 1;
            default :
                return 0;
        }
    }
    
    @POST
    @Path("/PlaceOrder")
    @Consumes("application/xml")
    @Produces("application/xml")
    public N6878296_Response PlaceOrder(N6878296_Order order) {
        N6878296_Response response = new N6878296_Response();
        response.setMessage("Order Successful");
        log("sus");
        return response;        
    }
}
