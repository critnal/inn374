package Services;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "entry")
public class N6878296_OrderEntry {
    
    private String stockItem;
    private int quantity;
    
    @XmlElement
    public String getStockItem() {
        return stockItem;
    }
    
    public void setStockItem(String stockItem) {
        this.stockItem = stockItem;
    }
    
    @XmlElement
    public int getQuantity() {
        return quantity;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
