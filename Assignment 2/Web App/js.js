var service = "http://localhost:11964/SalesService.svc/";

function requestQuote() {
    var vehicleRegistration = document.getElementById("vehicle-registration").value;
    var brand = document.getElementById("brand").value;
    var serviceType = document.getElementById("service-type").value;

    var url = service + "RequestQuote";
    url += "/" + vehicleRegistration;
    url += "/" + brand;
    url += "/" + serviceType;

    var request = doRequest(url, function () {
        receiveQuote(request.response);
    });
}

function receiveQuote(quote) {
    console.log("Quote: " + quote);
    var quoteidLabel = document.getElementById("quote-id");
    var quoteTotalCostLabel = document.getElementById("quote-total-cost");
    var quoteAmountOwedLabel = document.getElementById("quote-amount-owed");
    quoteidLabel.textContent = quote["ID"];
    quoteTotalCostLabel.textContent = quote["Cost"];
    quoteAmountOwedLabel.textContent = quote["AmountOwed"];

    hideDiv("quote-request-box");
    showDiv("quote-box");
    hideDiv("quote-delay-box");
    hideDiv("booking-response");
}

function acceptQuote() {
    var quoteID = document.getElementById("quote-id").textContent;
    var url = service + "AcceptQuote";
    url += "/" + quoteID;
    var request = doRequest(url, function () {
        console.log(request.response);
        document.getElementById("quote-delay").textContent = request.response["Days"] + " days";

        hideDiv("quote-request-box");
        hideDiv("quote-box");
        showDiv("quote-delay-box");
        hideDiv("booking-response");
    });
}

function cancelBooking() {
    var quoteID = document.getElementById("quote-id").textContent;
    var url = service + "CancelBooking";
    url += "/" + quoteID;
    var request = doRequest(url, function () {
        returnToFront();
    });
}

function doRequest(url, callback) {
    var request = new XMLHttpRequest();
    request.responseType = 'json';
    request.open("GET", url);
    request.onreadystatechange = function () {
        if (request.readyState != 4) { return; }
        callback();
    }
    request.send();
    return request;
}

function bookService() {
    var quoteID = document.getElementById("quote-id").textContent;
    var url = service + "BookService";
    url += "/" + quoteID;
    var request = doRequest(url, function () {
        var responseDiv = document.getElementById("booking-response-message");
        responseDiv.innerText = request.response["Message"];
        console.log(request);

        hideDiv("quote-request-box");
        hideDiv("quote-box");
        hideDiv("quote-delay-box");
        showDiv("booking-response");
    });
}

function returnToFront() {
    showDiv("quote-request-box");
    hideDiv("quote-box");
    hideDiv("quote-delay-box");
    hideDiv("booking-response");

}

function showDiv(divID) {
    document.getElementById(divID).style.display = "block";
}

function hideDiv(divID) {
    document.getElementById(divID).style.display = "none";
}